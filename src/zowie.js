// The MIT License (MIT)
//
// Copyright (c) 2015-Eternity Gerry Gold & Vera
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// @author gerry.gold@gmail.com
//
// MIT License
//
// How to Write and Read Using Browser's localStorage (ES6 Syntax):
//
//     import Zowie from 'zowie';
//     const Cache = new Zowie();
//
//     Cache.put('myKey', {data: {x: 1, y: 2, z: 3}});
//     const data = Cache.get('myKey');

'use strict';

export default class Zowie {

  constructor(isPersistent = true) {
    this.persistent = isPersistent;
    this._lootBag = {};

    // By default there is no timeout
    this._timeout = null;
  }

  set timeoutInSeconds(timeInSeconds) {
    this._timeout = timeInSeconds;
  }

  get timeoutInSeconds() {
    return this._timeout;
  }

  get persistent() {
    return this._persistent;
  }

  set persistent(isPersistent) {
    this._persistent = isPersistent;
  }

  put(key, val) {
    const item = {v: val, t: this._getNow()};

    if (this.persistent) {
      localStorage.setItem(key, JSON.stringify(item));
    } else {
      this._lootBag[key] = item;
    }
  }

  // If key does not exist:
  //   if 2nd arg is passed, i.e., default is defined, return default
  //   if 2nd arg is not passed, return null
  get(key, default_value = null) {
    if (this.persistent) { // local storage
      if (key in localStorage) {
        const obj = JSON.parse(localStorage.getItem(key));

        if (this._timeout === null) {
          return obj.v;
        } else {
          if (this._isCacheStale(obj.t)) {
            this.removeKey(key);
            return default_value;
          } else {
            return obj.v;
          }
        }
      } else {
        return default_value;
      }
    } else { // in memory
      if (key in this._lootBag) {
        if (this._timeout === null) {
          return this._lootBag[key].v;
        } else {
          if (this._isCacheStale(this._lootBag[key].t)) {
            this.removeKey(key);
            return default_value;
          } else {
            return this._lootBag[key].v;
          }
        }
      } else {
        return default_value;
      }
    }
  } // end get()

  // If a TTL is set and the key has expired, its existence is set free. :)
  keyExists(key) {
    if (this.persistent) { // local storage
      if (key in localStorage) {
        if (this._timeout === null) {
          return true;
        } else {
          const obj = JSON.parse(localStorage.getItem(key));
          if (this._isCacheStale(obj.t)) {
            this.removeKey(key);
            return false;
          } else {
            return true;
          }
        }
      } else {
        return false;
      }
    } else { // in memory
      if (key in this._lootBag) {
        if (this._timeout === null) {
          return true;
        } else {
          if (this._isCacheStale(this._lootBag[key].t)) {
            this.removeKey(key);
            return false;
          } else {
            return true;
          }
        }
      } else {
        return false;
      }
    }
  } // end keyExists

  removeKey(key) {
    if (this.persistent) {
      if (key in localStorage) {
        localStorage.removeItem(key);
      }
    } else {
      if (key in this._lootBag) {
        delete this._lootBag[key];
      }
    }
  }

  clear() {
    if (this.persistent) {
      localStorage.clear();
    } else {
      this._lootBag = {};
    }
  }

  keys() {
    return this.persistent ? Object.keys(localStorage) : Object.keys(this._lootBag);
  }

  isLocalStorageAvailable() {
    const key = new Date().getTime().toString();
    const val = key;

    try {
      localStorage.setItem(key, val);
      localStorage.removeItem(key);
      return true;
    } catch(ex) {
      return false;
    }
  }

  _getNow() {
    return parseInt(Date.now() / 1000, 10);
  }

  _isCacheStale(timestamp) {
    return (this._getNow() - timestamp) > this._timeout;
  }

}
