zowie
=====

Simple API for HTML5 Local Storage

Installation
------------

npm install zowie --save

Overview
--------

The subtle quirks of HTML5's Local Storage are replaced with a simple API that is
intuitive to use and just gets the job done.

You may be wondering: Isn't localStorage's native API already simple to use? Why
do I need to use Zowie at all? You certainly can use the native API, but you'll
quickly discover that: 1) omg, I can only save strings? wtf?! and 2) I can't set
an expiration time on the data? Again, wtf?! For you, my dear developer, Zowie
takes care of these limitations lickety-split.

Usage and Examples
------------------

Zowie has two modes of operation: persistent (the default) or not.

If persistent (Zowie's default mode), then the browser's localStorage mechanism
is used. Therefore, persistent mode is only for the browser. However, if
persistent is set to false, then Zowie instead uses in-memory storage and thus
can be used client or server side.

__Import Zowie Module and Instantiate Object__

There are three ways to instantiate the Zowie object, shown in the examples
below.


```javascript
// ES6 syntax (ES5 syntax example later in this document)
import Zowie from 'zowie';

// 1. a new Zowie object, no argument
//    will use localStorage
const Cache = new Zowie();


// 2. a new Zowie object with true arg
//    will use localStorage (explicit version of previous example)
const Cache = new Zowie(true);


// 3. a new Zowie object, with false arg
//    will not use localStorage, i.e., no persistence.
//    instantiating object this way allows use on the server.
const Cache = new Zowie(false);
```
__Store and Retrieve Data__

```javascript
import Zowie from 'zowie';
const Cache = new Zowie();

// store
Cache.put('myKey', {a: 1, b: true, stuff: {n: [2, 3, 5], composer: 'Stravinsky'}});

// retrieve
const myData = Cache.get('myKey');
```

__More About Retrieving Stored Data__

Inspired by Python's get() method, an optional 2nd arg is available.

```javascript
import Zowie from 'zowie';
const Cache = new Zowie();

// Retrieve a key which does not exist:
const myData = Cache.get('badKey'); // myData => null

// Retrieve a key which does not exist using an optional 2nd arg
const myData = Cache.get('badKey', {a: 1, b: 2}); // myData => {"a": 1, "b": 2}

```

__Setting A Timeout__

localStorage data typically remains available until it is manually deleted by
the user.

However, sometimes we want our storage cache to last for a specified duration of
time. Zowie allows a timeout value in seconds to be set. The timeout mechanism
works in both persistent or in-memory modes.

```javascript
import Zowie from 'zowie';
const Cache = new Zowie();

// After a key/value is Cache.put('myKey', myData), the data cannot be retrieved
// after 5 minutes has elapsed.
Cache.timeoutInSeconds = 300;
```

__Utilities__

`keyExists(key)` behaves thusly:

If no timeout is set (the default), then the method returns true if key exists,
false otherwise.

However, if a timeout has been set, then `keyExists()` method behaves slightly
differently. If no key found, returns false. If key exists and the timeout has
not yet expired, returns true; if key exists and timeout indicates the cache for
this key has expired, then the key is removed and returns false.

```javascript
Cache.keyExists(key); // => boolean
```

`removeKey(key)`, as its name suggests, merely removes the key if it exists. If the
key does not exist, it's a no-op.

```javascript
Cache.removeKey(key):
```

`clear()` removes all keys and data.

`keys()` returns a list of keys.

`isLocalStorageAvailable()` detects if localStorage is available.

This can happen, for example, if iPhone's Safari browser is in private mode, in
which case you can either alert the user to turn off private mode or just
reinstantiate the Zowie class with a false argument to use in-memory storage
instead of localStorage:

```javascript
Cache = new Zowie(false);
```

ES5: Instantiate the Zowie Class
--------------------------------

If you're in an ES5 context instead of ES6, you'll need to use `require` instead
of `import`.

The following assumes that you've got zowie v1.2.3+ installed. It uses the
transpiled version of zowie that comes with the package (`lib/index.js`). I
know, the following is a bit funky, but it should work.

```javscript
var Zowie = require('./node_modules/zowie/lib/index').default;
var Cache = new Zowie();
```

Credits
-------

Gerry Gold
April 2016

Have fun!
